import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
import axios from 'axios';
import moment from 'moment';

var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

class BarChart extends Component {
	constructor(props){
		super(props);
		this.updateChart = this.updateChart.bind(this);
		this.state = {
			dataChart : [],
			date:new Date,
		}
	}

	componentDidMount = () =>{
		setInterval(this.updateChart,600000);
		setInterval(this.updateTime,1000);
		this.fnInitData();
		
	}

	fnInitData = () =>{
		axios.get("https://disease.sh/v3/covid-19/countries?sort=cases").then(response=>{
			var listData = [];
			if(response.data && response.data.length > 0){
				listData = response.data.map((item,index)=>{return {"y":item.cases,"label":item.country}})
			}
			this.setState({
				dataChart:[
					...this.fnFilterTop10(listData)
				],
			})
		});
	}

	fnFilterTop10 = (dataAll) => {
		var filterTop10 = dataAll.filter((item,index)=>{return index<10});
		console.log(filterTop10)
		return filterTop10;
	}

	updateTime = () =>{
		this.setState({date:new Date})
	}

	updateChart() {
		this.fnInitData();
	}

	render() {
		var date = moment(this.state.date).format("DD MMM YYYY HH:mm:ss");
		const options = {
			height:500,
			animationEnabled: true, 
			theme: "light2",
			title:{
				text: date
			},
			axisX: {
				reversed: true,
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFormatter: ()=>{
					return " ";
				}
			},
			axisY: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFormatter: ()=>{
					return " ";
				}
			},
			data: [{
				type: "bar",
				indexLabel: "{label} ( {y} cases )",
				indexLabelFontColor: "black",
				indexLabelPlacement: "inside",
				dataPoints: this.state.dataChart,
			}]
		}
		
		return (
		<div>
			<h1>Covid Global Cases by SGN</h1>
			<CanvasJSChart options = {options} 
				onRef={ref => this.chart = ref}
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default BarChart;