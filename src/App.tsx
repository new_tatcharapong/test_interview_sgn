import * as React from 'react';
import BarChart from './view/column charts/Bar Chart';

function App() {
  return (
    <div style={{textAlign: 'center',}}>
      <BarChart>

      </BarChart>
      <h3>COVID-19 data sourced from Worldometers,updated every 10 minutes</h3>
    </div>
  );
}

export default App;